Ultimaker Original Plus - Dual Extruder
=======================================

(unofficial modification)

this project is a not tested yet!

![3D CAD Drawing](UMOplus_Dual-Extruder.png "3D CAD Drawing")


Parts to order
--------------

* 1x UMO Dual Extrusion Pack [(1x 229.00 EUR)](https://www.igo3d.com/de/ultimaker-original-dual-extruder-upgrade-kit.html)
* 2x UM2 Hot End Pack        [(2x  82.50 EUR)](https://www.igo3d.com/de/hot-end-pack.html)
* 2x UM2 Heater Cartridge    [(2x  48.00 EUR)](https://www.reichelt.de/Ersatzteile-und-Zubehoer-fuer-3D-Drucker/UM2-HEATER-CART/3/index.html?ACTION=3&LA=2&ARTICLE=150005&GROUPID=7355&artnr=UM2+HEATER+CART&SEARCH=%252A)


* 2x Fan 50x50mm 12VDC, 0.1A  (1.2W)
* 1x Fan 25x25mm  5VDC, 0.08A (0.4W)


### Shop

* https://ultimaker.com/en/products/add-ons
* https://ultimaker.com/en/products/spare-parts


Parts to build
--------------

Have a look in the repository for technical drawings.

* Cooling Rib Hot End   (AlCu4PbMgMn)
* Hot End Holder Top    (AlCu4PbMgMn)
* Hot End Holder Bottom (AlCu4PbMgMn)
* Dual Fan Bracket      (0.5mm Aluminum Sheet)


Original sources
----------------

### Repositories

* [Ultimaker Origianl](https://github.com/Ultimaker/UltimakerOriginal)
* [Ultimaker Origianl Plus](https://github.com/Ultimaker/Ultimaker-Original-Plus)
* [Ultimaker 2](https://github.com/Ultimaker/Ultimaker2)


### Parts

* [UMOplus - Print Head Hot End Holder (Part NR 1048)](https://github.com/Ultimaker/Ultimaker-Original-Plus/tree/master/1048_Print_Head_Hot_End_Holder)
* [UM2 - Hot End Holder Top (Part NR 1207)](https://github.com/Ultimaker/Ultimaker2/tree/master/1307_Hot_end_holder_top_(x1))
* [UM2 - Hot End Holder Bottom (Part NR 1306)](https://github.com/Ultimaker/Ultimaker2/tree/master/1306_Hot_end_holder_bottom_(x1))
* [UM2 - Cooling Rib Hot End (Part NR 1308)](https://github.com/Ultimaker/Ultimaker2/tree/master/1308_Cooling_Rib_Hot_End_(x1))
* [UM2 - Dual Fan Bracket (Part NR 1329)](https://github.com/Ultimaker/Ultimaker2/tree/master/1329_Dual_fan_bracket_(x1))
* [UM2 - Model Cooling Fan 12VDC 0.1A (Part NR 1313)](https://github.com/Ultimaker/Ultimaker2/tree/master/1313_Model_Cooling_Fan_12VDC_0.1_A_(x2))
* [UM2 - Hot End Cooling Fan 5VDC 0.08A (Part NR 1330)](https://github.com/Ultimaker/Ultimaker2/tree/master/1330_Hot-end_Cooling_Fan_5VDC_0.08A_(x1))
* [UM2 - Integrated Nozzle Heater Block 3mm Filament (Part NR 1301)](https://github.com/Ultimaker/Ultimaker2/tree/master/1301_Integrated_nozzle_heater_block_3mm_filament_(x1))

